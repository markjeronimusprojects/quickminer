/*
 * This file is part of QuickMiner.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.quickminer.util;

import org.bukkit.material.MaterialData;

/**
 * @author Mark Jeronimus
 */
// Created 2016-11-11
public final class MaterialDataOrError {
	private final MaterialData materialData;
	private final String       errorMessage;

	private MaterialDataOrError(MaterialData materialData, String errorMessage) {
		this.materialData = materialData;
		this.errorMessage = errorMessage;
	}

	public static MaterialDataOrError ofMaterialData(MaterialData materialData) {
		return new MaterialDataOrError(materialData, null);
	}

	public static MaterialDataOrError ofError(String errorMessage) {
		return new MaterialDataOrError(null, errorMessage);
	}

	public boolean hasError() {
		return errorMessage != null;
	}

	public MaterialData getMaterialData() { return materialData; }

	public String getErrorMessage()       { return errorMessage; }
}
