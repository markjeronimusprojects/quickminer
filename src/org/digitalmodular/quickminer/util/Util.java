/*
 * This file is part of QuickMiner.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.quickminer.util;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

/**
 * Generic utilities that don't fit anywhere else or are too fine grained to be put in a specialized utility class.
 *
 * @author Mark Jeronimus
 */
// Created 2016-11-04
@SuppressWarnings("deprecation") // It's ridiculous to deprecate MaterialData before releasing an alternative.
public enum Util {
	;

	public static void logInfo(String string) {
		Bukkit.getServer().getPluginManager().getPlugin("QuickMiner").getLogger().info(string);
	}

	public static void logWarning(String string) {
		Bukkit.getServer().getPluginManager().getPlugin("QuickMiner").getLogger().warning(string);
	}

	public static Location offsetLocation(Location location, double x, double y, double z) {
		return copyLocation(location).add(x, y, z);
	}

	public static Location copyLocation(Location location) {
		return new Location(location.getWorld(), location.getX(), location.getY(), location.getZ());
	}

	public static String shortBlockLocationString(Location location) {
		return location.getBlockX() + "," +
		       location.getBlockY() + ',' +
		       location.getBlockZ();
	}

	public static ItemStack getToolOfPlayer(Player player) {
//		ItemStack    tool            = player.getEquipment().getItemInMainHand();
//		ItemStack    tool            = player.getInventory().getItemInMainHand();
		return player.getItemInHand();
	}

	public static boolean listContainsMatchingBlock(List<MaterialData> list, MaterialData blockType) {
		for (MaterialData candidate : list)
			if (blocksMatch(candidate, blockType))
				return true;

		return false;
	}

	public static MaterialData tryGetMatchingBlockFromList(List<MaterialData> list, MaterialData blockType) {
		for (MaterialData candidate : list)
			if (blocksMatch(candidate, blockType))
				return candidate;

		throw new IllegalArgumentException("Block type not in list: " + blockType + " ∉ " + list);
	}

	public static boolean blocksMatch(MaterialData blockType1, MaterialData blockType2) {
		if (blockType1.getItemType() != blockType2.getItemType())
			return false;

		if (blockType1.getData() == -1 || blockType2.getData() == -1)
			return true;

		return blockType1.getData() == blockType2.getData();
	}

	public static boolean minableBlockAt(Location location, List<MaterialData> minableBlockTypes) {
		Block        block     = location.getBlock();
		MaterialData blockType = new MaterialData(block.getType(), block.getData());
		return listContainsMatchingBlock(minableBlockTypes, blockType);
	}
}
