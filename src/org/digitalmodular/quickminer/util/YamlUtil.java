/*
 * This file is part of QuickMiner.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.quickminer.util;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Utilities for converting and validating data structures returned by a YAML reader (e.g. config.yml).
 *
 * @author Mark Jeronimus
 */
// Created 2016-11-05
public enum YamlUtil {
	;

	public static List<List<String>> convertToStringListList(String section, Object possibleStringListList) {
		List<?> list = convertToList(section, possibleStringListList);

		for (int i = 0; i < list.size(); i++)
			convertToStringList(section + '.' + i, list.get(i));

		@SuppressWarnings("unchecked")
		List<List<String>> stringListList = (List<List<String>>)list;
		return stringListList;
	}

	public static List<String> convertToStringList(String section, Object possibleStringList) {
		List<?> list = convertToList(section, possibleStringList);

		for (int j = 0; j < list.size(); j++)
			convertToString(section + '.' + j, list.get(j));

		@SuppressWarnings("unchecked")
		List<String> stringList = (List<String>)list;
		return stringList;
	}

	public static List<Map<String, Object>> convertToMapList(String section, Object possibleMapList) {
		List<?> list = convertToList(section, possibleMapList);

		for (int i = 0; i < list.size(); i++)
			convertToMap(section + '.' + i, list.get(i));

		@SuppressWarnings("unchecked")
		List<Map<String, Object>> mapList = (List<Map<String, Object>>)list;
		return mapList;
	}

	public static int convertToInt(String section, Object possibleInt) {
		if (!(possibleInt instanceof Integer))
			throw new IllegalArgumentException(section + " is not an integer: " +
			                                   possibleInt.getClass().getSimpleName());

		return (Integer)possibleInt;
	}

	public static boolean convertToBoolean(String section, Object possibleBoolean) {
		if (!(possibleBoolean instanceof Boolean))
			throw new IllegalArgumentException(section + " is not a boolean: " +
			                                   possibleBoolean.getClass().getSimpleName());

		return (Boolean)possibleBoolean;
	}

	public static String convertToString(String section, Object possibleString) {
		if (!(possibleString instanceof String))
			throw new IllegalArgumentException(section + " is not a string: " +
			                                   possibleString.getClass().getSimpleName());

		return (String)possibleString;
	}

	private static List<?> convertToList(String section, Object possibleMapList) {
		if (possibleMapList == null)
			return Collections.emptyList();

		if (!(possibleMapList instanceof List<?>))
			throw new IllegalArgumentException(section + " is not a list: " +
			                                   possibleMapList.getClass().getSimpleName());

		return (List<?>)possibleMapList;
	}

	public static Map<?, ?> convertToMap(String section, Object possibleMap) {
		if (!(possibleMap instanceof Map<?, ?>))
			throw new IllegalArgumentException(section + " is not a map: " + possibleMap.getClass().getSimpleName());

		return (Map<?, ?>)possibleMap;
	}

	public static <T extends Enum<T>> T convertToEnum(String section, Class<T> enumType, Object possibleEnum) {
		String string = convertToString(section, possibleEnum);

		try {
			return Enum.valueOf(enumType, string);
		} catch (IllegalArgumentException ex) {
			throw new IllegalArgumentException(section + " is not valid: " + ex.getMessage(), ex);
		}
	}
}
