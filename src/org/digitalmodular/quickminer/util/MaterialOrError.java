/*
 * This file is part of QuickMiner.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.quickminer.util;

import org.bukkit.Material;

/**
 * @author Mark Jeronimus
 */
// Created 2016-11-11
public final class MaterialOrError {
	private final Material material;
	private final String   errorMessage;

	private MaterialOrError(Material material, String errorMessage) {
		this.material = material;
		this.errorMessage = errorMessage;
	}

	public static MaterialOrError ofMaterial(Material material) {
		return new MaterialOrError(material, null);
	}

	public static MaterialOrError ofError(String errorMessage) {
		return new MaterialOrError(null, errorMessage);
	}

	public boolean hasError() {
		return errorMessage != null;
	}

	public Material getMaterial()   { return material; }

	public String getErrorMessage() { return errorMessage; }
}
