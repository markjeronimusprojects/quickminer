/*
 * This file is part of QuickMiner.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.quickminer.versioning;

import net.minecraft.server.v1_7_R1.Block;
import net.minecraft.server.v1_7_R1.EntityPlayer;
import net.minecraft.server.v1_7_R1.Item;
import net.minecraft.server.v1_7_R1.Packet;
import net.minecraft.server.v1_7_R1.PacketPlayOutNamedSoundEffect;
import net.minecraft.server.v1_7_R1.PlayerConnection;
import net.minecraft.server.v1_7_R1.PlayerInteractManager;
import org.bukkit.craftbukkit.v1_7_R1.entity.CraftHumanEntity;
import org.bukkit.craftbukkit.v1_7_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_7_R1.util.CraftMagicNumbers;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * @author Mark Jeronimus
 */
// Created 2016-11-07
@SuppressWarnings("deprecation") // It's ridiculous to deprecate CraftMagicNumbers before releasing an alternative.
public class NMSTools_v1_7_R1 implements NMSTools {
	@Override
	public double getEyeHeight(Player player) {
		return ((CraftHumanEntity)player).getHandle().getBukkitEntity().getEyeHeight();
	}

	@Override
	public int getBlockID(String name) {
		Block block = Block.b(name);
		if (block == null)
			return -1;

		return CraftMagicNumbers.getId(block);
	}

	@Override
	public int getItemID(String name) {
		Item item = getByName(name);
		if (item == null)
			return -1;

		return CraftMagicNumbers.getId(item);
	}

	public static Item getByName(String name) {
		Item item = (Item)Item.REGISTRY.a(name);

		if (item != null)
			return item;

		try {
			return Item.d(Integer.parseInt(name));
		} catch (NumberFormatException ignored) {
			return null;
		}
	}

	@Override
	public void breakBlock(Player player, Location location) {
		// Thanks to Parker Hawke for finding this technique
		EntityPlayer          playerEntity = ((CraftPlayer)player).getHandle();
		PlayerInteractManager manager      = playerEntity.playerInteractManager;
		manager.breakBlock(location.getBlockX(), location.getBlockY(), location.getBlockZ());
	}

	@Override
	public void playToolBreakSound(Player player) {
		Location location = player.getLocation();
		Packet packet = new PacketPlayOutNamedSoundEffect("random.break",
		                                                  location.getX(),
		                                                  location.getY(),
		                                                  location.getZ(),
		                                                  1.0f,
		                                                  1.0f);

		sendPacket(player, packet);
	}

	private static void sendPacket(Player player, Packet packet) {
		EntityPlayer     playerEntity = ((CraftPlayer)player).getHandle();
		PlayerConnection connection   = playerEntity.playerConnection;
		connection.sendPacket(packet);
	}
}
