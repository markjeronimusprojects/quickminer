/*
 * This file is part of QuickMiner.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.quickminer.function;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.bukkit.Material;

import org.digitalmodular.quickminer.util.MaterialOrError;

/**
 * Converts a list of Minecraft item names (e.g. "minecraft:iron_shovel" or "256") to the Bukkit Plugin API's
 * {@link Material} (e.g. IRON_SPADE).
 *
 * @author Mark Jeronimus
 */
// Created 2016-11-08
public class NMSToSpigotItemListConverter implements Function<List<String>, List<MaterialOrError>> {
	private final NMSToSpigotItemConverter nmsToSpigotItemConverter;

	public NMSToSpigotItemListConverter(NMSToSpigotItemConverter nmsToSpigotItemConverter) {
		this.nmsToSpigotItemConverter = nmsToSpigotItemConverter;
	}

	@Override
	public List<MaterialOrError> apply(List<String> minecraftNames) {
		return minecraftNames.stream()
		                     .map(nmsToSpigotItemConverter)
		                     .collect(Collectors.toList());
	}
}
