/*
 * This file is part of QuickMiner.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.quickminer.function;

import java.util.List;
import java.util.function.UnaryOperator;

import org.bukkit.material.MaterialData;

import org.digitalmodular.quickminer.config.QuickMinerConfig;
import org.digitalmodular.quickminer.util.Util;

/**
 * Returns a block that is the the canonical equivalent of the given block.
 * <p>
 * A canonical block is the first block in any list of same blocks, as defined in the config.
 *
 * @author Mark Jeronimus
 */
// Created 2016-11-08
public class BlockCanonicalizer implements UnaryOperator<MaterialData> {
	private final QuickMinerConfig config;

	public BlockCanonicalizer(QuickMinerConfig config) {
		this.config = config;
	}

	@Override
	public MaterialData apply(MaterialData blockType) {
		for (List<MaterialData> list : config.getSameBlocks())
			if (Util.listContainsMatchingBlock(list, blockType)) return list.get(0);

		return blockType;
	}
}
