/*
 * This file is part of QuickMiner.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.quickminer.function;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.bukkit.material.MaterialData;

import org.digitalmodular.quickminer.util.MaterialDataOrError;

/**
 * Converts a list of Minecraft block names (e.g. "minecraft:stone 1" or "1 1") to the Bukkit Plugin API's
 * {@link MaterialData} (e.g. STONE(1)).
 *
 * @author Mark Jeronimus
 */
// Created 2016-11-08
public class NMSToSpigotBlockListConverter implements Function<List<String>, List<MaterialDataOrError>> {
	private final NMSToSpigotBlockConverter nmsToSpigotBlockConverter;

	public NMSToSpigotBlockListConverter(NMSToSpigotBlockConverter nmsToSpigotBlockConverter) {
		this.nmsToSpigotBlockConverter = nmsToSpigotBlockConverter;
	}

	@Override
	public List<MaterialDataOrError> apply(List<String> minecraftNames) {
		return minecraftNames.stream()
		                     .map(nmsToSpigotBlockConverter)
		                     .collect(Collectors.toList());
	}
}
