/*
 * This file is part of QuickMiner.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.quickminer.function;

import java.util.Optional;
import java.util.function.BiFunction;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

import org.digitalmodular.quickminer.config.MineType;
import org.digitalmodular.quickminer.config.QuickMinerConfig;
import org.digitalmodular.quickminer.util.Util;

/**
 * Given the tool and type of block that the player started mining, finds the matching MineType in the config.
 *
 * @author Mark Jeronimus
 */
// Created 2016-11-10
public class MineTypeFinder implements BiFunction<ItemStack, MaterialData, Optional<MineType>> {
	private final QuickMinerConfig config;

	public MineTypeFinder(QuickMinerConfig config) {
		this.config = config;
	}

	@Override
	public Optional<MineType> apply(ItemStack tool, MaterialData blockType) {
		Material toolType  = tool.getType();
		boolean  silkTouch = tool.getEnchantmentLevel(Enchantment.SILK_TOUCH) > 0;

		for (MineType mineType : config.getMineTypes()) {
			if (mineType.isRequireSilkTouch() && !silkTouch) continue;
			if (!mineType.getTools().contains(toolType)) continue;
			if (!Util.listContainsMatchingBlock(mineType.getBlocks(), blockType)) continue;

			return Optional.of(mineType);
		}

		return Optional.empty();
	}
}
