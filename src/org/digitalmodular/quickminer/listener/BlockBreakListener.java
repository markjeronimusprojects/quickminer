/*
 * This file is part of QuickMiner.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.quickminer.listener;

import java.util.List;
import java.util.Optional;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

import org.digitalmodular.quickminer.config.MineType;
import org.digitalmodular.quickminer.config.QuickMinerConfig;
import org.digitalmodular.quickminer.function.BlockCanonicalizer;
import org.digitalmodular.quickminer.function.MineTypeFinder;
import org.digitalmodular.quickminer.minealgorithm.DefaultMinerAlgorithm;
import org.digitalmodular.quickminer.minealgorithm.MinerAlgorithm;
import org.digitalmodular.quickminer.util.Util;
import org.digitalmodular.quickminer.versioning.NMSTools;

/**
 * @author Mark Jeronimus
 */
// Created 2016-11-03
@SuppressWarnings("deprecation") // It's ridiculous to deprecate MaterialData before releasing an alternative.
public class BlockBreakListener implements Listener {
	private final NMSTools           nmsTools;
	private final QuickMinerConfig   config;
	private final BlockMiner         blockMiner;
	private final BlockCanonicalizer blockCanonicalizer;
	private final MineTypeFinder     mineTypeFinder;

	private Player       player;
	private Location     playerLocation;
	private Location     digLocation;
	private MineType     mineType;
	private MaterialData mineBlockType;

	private boolean recursiveCall;

	public BlockBreakListener(NMSTools nmsTools, QuickMinerConfig config,
	                          BlockMiner blockMiner,
	                          BlockCanonicalizer blockCanonicalizer,
	                          MineTypeFinder mineTypeFinder) {
		this.nmsTools = nmsTools;
		this.config = config;
		this.blockMiner = blockMiner;
		this.blockCanonicalizer = blockCanonicalizer;
		this.mineTypeFinder = mineTypeFinder;
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		if (recursiveCall) return;

		player = e.getPlayer();
		if (!playerMayDig()) return;

		Block block = e.getBlock();

		playerLocation = player.getLocation();
		digLocation = block.getLocation();

		ItemStack    tool            = Util.getToolOfPlayer(player);
		Material     toolType        = tool.getType();
		MaterialData blockType       = new MaterialData(block.getType(), block.getData());
		MaterialData uniqueBlockType = blockCanonicalizer.apply(blockType);

		Optional<MineType> optionalMineType = mineTypeFinder.apply(tool, uniqueBlockType);
		if (!optionalMineType.isPresent()) return;
		mineType = optionalMineType.get();

		mineBlockType = Util.tryGetMatchingBlockFromList(mineType.getBlocks(), uniqueBlockType);

		List<Location> blockLocations = findVein();

		Util.logInfo("Mining " + blockType +
		             " at " + Util.shortBlockLocationString(digLocation) +
		             " with " + toolType +
		             " (" + blockLocations.size() + " blocks)");

		try {
			recursiveCall = true;
			blockMiner.mine(player, mineType, blockLocations);
		} finally {
			recursiveCall = false;
		}
	}

	private boolean playerMayDig() {
		return player.getGameMode() == GameMode.SURVIVAL || player.getGameMode() == GameMode.ADVENTURE;
	}

	private List<Location> findVein() {
		MinerAlgorithm minerAlgorithm = getMinerAlgorithm();

		double eyeHeight = nmsTools.getEyeHeight(player);

		Location           playerBlockLocation = Util.offsetLocation(playerLocation, -0.5, -0.5, -0.5);
		Location           centerLocation      = Util.offsetLocation(playerLocation, 0, eyeHeight, 0);
		List<MaterialData> minableBlockTypes   = config.getMinableBlocks(mineBlockType);
		int                maxCount            = mineType.getMaxCount();

		return minerAlgorithm.calculateVein(centerLocation,
		                                    Util.copyLocation(digLocation),
		                                    maxCount,
		                                    minableBlockTypes);
	}

	private MinerAlgorithm getMinerAlgorithm() {
		return new DefaultMinerAlgorithm();
	}
}
