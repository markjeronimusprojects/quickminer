/*
 * This file is part of QuickMiner.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.quickminer.listener;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import org.digitalmodular.quickminer.config.MineType;
import org.digitalmodular.quickminer.versioning.NMSTools;

/**
 * An algorithm that takes a vein and tries to mine it block by block until a stop condition is met.
 *
 * @author Mark Jeronimus
 */
// Created 2016-11-07
@SuppressWarnings("deprecation") // It's ridiculous to deprecate MaterialData before releasing an alternative.
public class BlockMiner {
	private final NMSTools nmsTools;

	public BlockMiner(NMSTools nmsTools) {
		this.nmsTools = nmsTools;
	}

	/**
	 * @param player   The player that mines
	 * @param mineType The configuration for this vein (contains stop conditions)
	 * @param vein     The blocks to mine
	 */
	public void mine(Player player, MineType mineType, List<Location> vein) {
		ItemStack tool      = player.getItemInHand();
		int       maxDamage = getMaxDamage(mineType, tool);

		for (Location location : vein) {
			if (tool.getDurability() > maxDamage || tool.getAmount() == 0) break;

			nmsTools.breakBlock(player, location);
		}

		if (tool.getDurability() > maxDamage)
			nmsTools.playToolBreakSound(player);
	}

	private static int getMaxDamage(MineType mineType, ItemStack tool) {
		int maxDamage = tool.getType().getMaxDurability();

		switch (mineType.getStopBeforeToolBreaks()) {
			case NEVER:
				break;
			case ALWAYS:
				maxDamage--;
				break;
			case WHEN_ENCHANTED:
				if (!tool.getEnchantments().isEmpty())
					maxDamage--;
				break;
		}

		return maxDamage;
	}
}
