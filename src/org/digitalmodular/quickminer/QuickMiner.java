/*
 * This file is part of QuickMiner.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.quickminer;

import org.bukkit.plugin.java.JavaPlugin;

import org.digitalmodular.quickminer.config.QuickMinerConfig;
import org.digitalmodular.quickminer.config.QuickMinerConfigReader;
import org.digitalmodular.quickminer.function.BlockCanonicalizer;
import org.digitalmodular.quickminer.function.BlockListCanonicalizer;
import org.digitalmodular.quickminer.function.MineTypeFinder;
import org.digitalmodular.quickminer.function.NMSToSpigotBlockConverter;
import org.digitalmodular.quickminer.function.NMSToSpigotBlockListConverter;
import org.digitalmodular.quickminer.function.NMSToSpigotItemConverter;
import org.digitalmodular.quickminer.function.NMSToSpigotItemListConverter;
import org.digitalmodular.quickminer.listener.BlockBreakListener;
import org.digitalmodular.quickminer.listener.BlockMiner;
import org.digitalmodular.quickminer.versioning.NMSTools;
import org.digitalmodular.quickminer.versioning.NMSUtil;

/**
 * @author Mark Jeronimus
 */
// Created 2016-10-23
public class QuickMiner extends JavaPlugin {
	private QuickMinerConfig       config;
	private QuickMinerConfigReader configReader;
	private BlockBreakListener     blockBreakListener;

	@Override
	public void onEnable() {

		NMSTools nmsTools = NMSUtil.getInstance();

		config = new QuickMinerConfig();
		NMSToSpigotItemConverter      itemConverter          = new NMSToSpigotItemConverter(nmsTools);
		NMSToSpigotItemListConverter  itemListConverter      = new NMSToSpigotItemListConverter(itemConverter);
		NMSToSpigotBlockConverter     blockConverter         = new NMSToSpigotBlockConverter(nmsTools);
		NMSToSpigotBlockListConverter blockListConverter     = new NMSToSpigotBlockListConverter(blockConverter);
		BlockCanonicalizer            blockCanonicalizer     = new BlockCanonicalizer(config);
		BlockListCanonicalizer        blockListCanonicalizer = new BlockListCanonicalizer(blockCanonicalizer);
		BlockMiner                    blockMiner             = new BlockMiner(nmsTools);
		MineTypeFinder                mineTypeFinder         = new MineTypeFinder(config);
		configReader = new QuickMinerConfigReader(itemListConverter, blockListConverter, blockListCanonicalizer);
		blockBreakListener = new BlockBreakListener(nmsTools, config, blockMiner, blockCanonicalizer, mineTypeFinder);

		loadConfig();

		getLogger().info("Configuration loaded");

		registerListeners();

		getLogger().info("Enabled");
	}

	@Override
	public void onDisable() {
		getLogger().info("Disabled");
	}

	private void loadConfig() {
		saveDefaultConfig();

		config.read(getConfig(), configReader);
	}

	private void registerListeners() {
		getServer().getPluginManager().registerEvents(blockBreakListener, this);
	}
}
