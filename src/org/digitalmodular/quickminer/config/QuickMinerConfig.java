/*
 * This file is part of QuickMiner.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.quickminer.config;

import java.util.Collections;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.material.MaterialData;

import org.digitalmodular.quickminer.util.Util;

/**
 * @author Mark Jeronimus
 */
// Created 2016-11-04
public class QuickMinerConfig {
	private List<MineType>           mineTypes;
	private List<List<MaterialData>> sameBlocks;

	public void read(FileConfiguration config, QuickMinerConfigReader configReader) {
		readStaticData(config, configReader);
		optimizeDataInMemory(configReader);

		mineTypes = Collections.unmodifiableList(mineTypes);
		sameBlocks = Collections.unmodifiableList(sameBlocks);
	}

	private void readStaticData(FileConfiguration config, QuickMinerConfigReader configReader) {
		mineTypes = configReader.loadMineTypes(config, "MineTypes");
		sameBlocks = configReader.loadSameBlocks(config, "SameTypeOfBlocks");
	}

	private void optimizeDataInMemory(QuickMinerConfigReader configReader) {
		mineTypes = configReader.canonicalizeBlocksInMineTypes(mineTypes);
	}

	public List<MaterialData> getMinableBlocks(MaterialData blockType) {
		for (List<MaterialData> list : sameBlocks)
			if (Util.listContainsMatchingBlock(list, blockType)) return list;

		return Collections.singletonList(blockType);
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<MineType> getMineTypes() { return mineTypes; }

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<List<MaterialData>> getSameBlocks() { return sameBlocks; }
}
