/*
 * This file is part of QuickMiner.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.quickminer.config;

import java.util.Collections;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.material.MaterialData;

/**
 * A type of vein that can be mined, as defined in te config.
 *
 * @author Mark Jeronimus
 */
// Created 2016-11-04
public class MineType {
	public enum StopBeforeToolBreaks {
		NEVER,
		ALWAYS,
		WHEN_ENCHANTED;
	}

	private final List<Material>       tools;
	private final List<MaterialData>   blocks;
	private final int                  maxCount;
	private final StopBeforeToolBreaks stopBeforeToolBreaks;
	private final boolean              requireSilkTouch;

	public MineType(List<Material> tools,
	                List<MaterialData> blocks,
	                int maxCount,
	                StopBeforeToolBreaks stopBeforeToolBreaks,
	                boolean requireSilkTouch) {
		this.tools = Collections.unmodifiableList(tools);
		this.blocks = Collections.unmodifiableList(blocks);
		this.maxCount = maxCount;
		this.stopBeforeToolBreaks = stopBeforeToolBreaks;
		this.requireSilkTouch = requireSilkTouch;
	}

	public List<Material> getTools()                      { return Collections.unmodifiableList(tools); }

	public List<MaterialData> getBlocks()                 { return Collections.unmodifiableList(blocks); }

	public int getMaxCount()                              { return maxCount; }

	public StopBeforeToolBreaks getStopBeforeToolBreaks() { return stopBeforeToolBreaks; }

	public boolean isRequireSilkTouch()                   { return requireSilkTouch; }

	public MineType setBlocks(List<MaterialData> blocks) {
		return new MineType(tools, blocks, maxCount, stopBeforeToolBreaks, requireSilkTouch);
	}
}
