/*
 * This file is part of QuickMiner.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.quickminer.config;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.material.MaterialData;

import org.digitalmodular.quickminer.function.BlockListCanonicalizer;
import org.digitalmodular.quickminer.function.NMSToSpigotBlockListConverter;
import org.digitalmodular.quickminer.function.NMSToSpigotItemListConverter;
import org.digitalmodular.quickminer.util.MaterialDataOrError;
import org.digitalmodular.quickminer.util.MaterialOrError;
import org.digitalmodular.quickminer.util.Util;
import org.digitalmodular.quickminer.util.YamlUtil;
import static org.digitalmodular.quickminer.config.MineType.StopBeforeToolBreaks;

/**
 * @author Mark Jeronimus
 */
// Created 2016-11-07
public class QuickMinerConfigReader {
	private final NMSToSpigotItemListConverter  nmsToSpigotItemListConverter;
	private final NMSToSpigotBlockListConverter nmsToSpigotBlockListConverter;
	private final BlockListCanonicalizer        blockListCanonicalizer;

	public QuickMinerConfigReader(NMSToSpigotItemListConverter nmsToSpigotItemListConverter,
	                              NMSToSpigotBlockListConverter nmsToSpigotBlockListConverter,
	                              BlockListCanonicalizer blockListCanonicalizer) {
		this.nmsToSpigotItemListConverter = nmsToSpigotItemListConverter;
		this.nmsToSpigotBlockListConverter = nmsToSpigotBlockListConverter;
		this.blockListCanonicalizer = blockListCanonicalizer;
	}

	public List<MineType> loadMineTypes(FileConfiguration config, String section) {
		Object data = config.get(section);

		List<Map<String, Object>> mineTypesYaml = YamlUtil.convertToMapList("Section " + section, data);

		return convertLoadedMineTypeLists(mineTypesYaml);
	}

	public List<List<MaterialData>> loadSameBlocks(FileConfiguration config, String section) {
		Object data = config.get(section);

		List<List<String>> sameBlockLists = YamlUtil.convertToStringListList("Section " + section, data);

		return convertLoadedSameBlocksLists(sameBlockLists);
	}

	public List<MineType> canonicalizeBlocksInMineTypes(List<MineType> mineTypes) {
		return mineTypes.stream()
		                .map(this::canonicalizeBlocksInMineType)
		                .collect(Collectors.toList());
	}

	private List<MineType> convertLoadedMineTypeLists(List<Map<String, Object>> mineTypes) {
		return mineTypes.stream()
		                .map(this::convertLoadedMineType)
		                .collect(Collectors.toList());
	}

	private List<List<MaterialData>> convertLoadedSameBlocksLists(List<List<String>> sameBlockLists) {
		return sameBlockLists.stream()
		                     .map(this::convertLoadedSameBlocks)
		                     .collect(Collectors.toList());
	}

	private List<MaterialData> convertLoadedSameBlocks(List<String> names) {
		List<MaterialDataOrError> blocksOrErrors = nmsToSpigotBlockListConverter.apply(names);
		List<MaterialData>        blocks         = checkBlocksForErrors(blocksOrErrors);
		return Collections.unmodifiableList(blocks);
	}

	private MineType convertLoadedMineType(Map<String, Object> map) {
		List<Material>       tools                = null;
		List<MaterialData>   blocks               = null;
		Integer              maxCount             = null;
		StopBeforeToolBreaks stopBeforeToolBreaks = null;
		boolean              requireSilkTouch     = false;

		for (Map.Entry<String, Object> entry : map.entrySet()) {
			String key   = entry.getKey();
			Object value = entry.getValue();

			switch (key) {
				case "Tools":
					List<String> toolStrings = YamlUtil.convertToStringList("A " + key + " entry", value);
					List<MaterialOrError> toolsOrErrors = nmsToSpigotItemListConverter.apply(toolStrings);
					tools = checkToolsForErrors(toolsOrErrors);
					break;
				case "Blocks":
					List<String> blockStrings = YamlUtil.convertToStringList("A " + key + " entry", value);
					List<MaterialDataOrError> blocksOrErrors = nmsToSpigotBlockListConverter.apply(blockStrings);
					blocks = checkBlocksForErrors(blocksOrErrors);
					break;
				case "MaxCount":
					maxCount = YamlUtil.convertToInt("A " + key + " entry", value);
					break;
				case "StopBeforeToolBreaks":
					stopBeforeToolBreaks = YamlUtil.convertToEnum("A " + key + " entry",
					                                              StopBeforeToolBreaks.class,
					                                              value);
					break;
				case "RequireSilkTouch":
					requireSilkTouch = YamlUtil.convertToBoolean("A " + key + " entry", value);
					break;
				default:
					throw new ConfigurationException("A MineTypes entry contains an invalid section: " + key);
			}
		}

		checkForMissingKeys(tools, blocks, maxCount, stopBeforeToolBreaks);
		validateValues(tools, blocks, maxCount);

		return new MineType(tools, blocks, maxCount, stopBeforeToolBreaks, requireSilkTouch);
	}

	private static List<Material> checkToolsForErrors(List<MaterialOrError> toolsOrErrors) {
		Map<Boolean, List<MaterialOrError>> split = toolsOrErrors.stream()
		                                                         .collect(Collectors.partitioningBy(
				                                                         MaterialOrError::hasError));
		List<MaterialOrError> tools  = split.get(false);
		List<MaterialOrError> errors = split.get(true);

		errors.stream()
		      .map(MaterialOrError::getErrorMessage)
		      .forEach(Util::logWarning);

		return tools.stream()
		            .map(MaterialOrError::getMaterial)
		            .distinct()
		            .collect(Collectors.toList());
	}

	private static List<MaterialData> checkBlocksForErrors(List<MaterialDataOrError> toolsOrErrors) {
		Map<Boolean, List<MaterialDataOrError>> split = toolsOrErrors.stream()
		                                                             .collect(Collectors.partitioningBy(
				                                                             MaterialDataOrError::hasError));
		List<MaterialDataOrError> blocks = split.get(false);
		List<MaterialDataOrError> errors = split.get(true);

		errors.stream()
		      .map(MaterialDataOrError::getErrorMessage)
		      .forEach(Util::logWarning);

		return blocks.stream()
		             .map(MaterialDataOrError::getMaterialData)
		             .distinct()
		             .collect(Collectors.toList());
	}

	private static void checkForMissingKeys(List<Material> tools,
	                                        List<MaterialData> blocks,
	                                        Integer maxCount,
	                                        StopBeforeToolBreaks stopBeforeToolBreaks) {
		if (tools == null)
			throw new ConfigurationException("A MineTypes entry is missing a section Tools");
		if (blocks == null)
			throw new ConfigurationException("A MineTypes entry is missing a section Blocks");
		if (maxCount == null)
			throw new ConfigurationException("A MineTypes entry is missing a section MaxCount");
		if (stopBeforeToolBreaks == null)
			throw new ConfigurationException("A MineTypes entry is missing a section StopBeforeToolBreaks");
	}

	private static void validateValues(List<Material> tools, List<MaterialData> blocks, Integer maxCount) {
		if (tools.isEmpty())
			throw new ConfigurationException("A Tools section is empty");
		if (blocks.isEmpty())
			throw new ConfigurationException("A Blocks section is empty");
		if (maxCount < 1)
			throw new ConfigurationException("A MaxCount value is less than one: " + maxCount);
		if (maxCount > 10240)
			throw new ConfigurationException("A MaxCount value is larger than the maximum of 1024: " + maxCount);
	}

	private MineType canonicalizeBlocksInMineType(MineType mineType) {
		List<MaterialData> blocks       = mineType.getBlocks();
		List<MaterialData> uniqueBlocks = blockListCanonicalizer.apply(blocks);
		return mineType.setBlocks(uniqueBlocks);
	}
}
