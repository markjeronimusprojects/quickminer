/*
 * This file is part of QuickMiner.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.quickminer.minealgorithm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.material.MaterialData;

import org.digitalmodular.quickminer.util.Util;

/**
 * A MinerAlgorithm that attempts to create a dome (half-ball) shaped vein.
 *
 * @author Mark Jeronimus
 */
// Created 2016-11-06
public class DomeFillAlgorithm implements MinerAlgorithm {
	// (Ab)Use the yaw field to store the distance to the centerLocation.
	private static final Comparator<Location> SORT_BY_YAW =
			(candidate1, candidate2) -> Float.compare(candidate1.getYaw(), candidate2.getYaw());

	private final Queue<Location> backTrackingQueue = new PriorityQueue<>(256, SORT_BY_YAW);
	private final Queue<Location> reserveQueue      = new PriorityQueue<>(256, SORT_BY_YAW);

	private Location           centerLocation;
	private Location           digLocation;
	private int                maxCount;
	private List<MaterialData> minableBlockTypes;

	private List<Location> vein;
	private Set<Location> ignoreList = Collections.emptySet();

	private static boolean isOutOfRange(Location candidate) {
		return candidate.getY() < 0 || candidate.getY() >= 256;
	}

	public void addToIgnoreList(Collection<Location> ignoreList) {
		if (ignoreList.isEmpty()) return;

		if (this.ignoreList.isEmpty())
			this.ignoreList = new HashSet<>(Math.max(256, ignoreList.size() * 2));

		this.ignoreList.addAll(ignoreList);
	}

	@Override
	public List<Location> calculateVein(Location centerLocation,
	                                    Location digLocation,
	                                    int maxCount,
	                                    List<MaterialData> minableBlockTypes) {
		this.centerLocation = centerLocation;
		this.digLocation = digLocation;
		this.maxCount = maxCount;
		//noinspection AssignmentToCollectionOrArrayFieldFromParameter // Parameter minableBlockTypes is immutable
		this.minableBlockTypes = minableBlockTypes;

		vein = new ArrayList<>(maxCount);

		testNearbyBlock(digLocation, 0, 0, 0);
		if (backTrackingQueue.isEmpty())
			return Collections.emptyList();

		floodFill();

		backTrackingQueue.clear();
		reserveQueue.removeAll(vein);
		return Collections.unmodifiableList(vein);
	}

	public Location tryGetFromReserveQueue() {
		return reserveQueue.poll();
	}

	private void floodFill() {
		do {
			if (vein.size() == maxCount) return;

			Location location = backTrackingQueue.remove();
			vein.add(location);

			testForMoreBlocks(location);
		} while (!backTrackingQueue.isEmpty());
	}

	private void testForMoreBlocks(Location location) {
		testNearbyBlock(location, -1, 0, 0);
		testNearbyBlock(location, 1, 0, 0);
		testNearbyBlock(location, 0, -1, 0);
		testNearbyBlock(location, 0, 1, 0);
		testNearbyBlock(location, 0, 0, -1);
		testNearbyBlock(location, 0, 0, 1);
	}

	private void testNearbyBlock(Location location, int dx, int dy, int dz) {
		Location candidate = makeCandidate(location, dx, dy, dz);

		if (candidateIsValid(candidate)) {
			if (isToBeIncluded(candidate, dx, dy, dz))
				backTrackingQueue.add(candidate);
			else
				reserveQueue.add(candidate);
		}
	}

	private Location makeCandidate(Location location, int dx, int dy, int dz) {
		double x = location.getX() + dx;
		double y = location.getY() + dy;
		double z = location.getZ() + dz;

		Location candidate = new Location(location.getWorld(), x, y, z);
		candidate.setYaw((float)getSortOrder(candidate));

		return candidate;
	}

	private double getSortOrder(Location location) {
		double px = centerLocation.getX() - location.getX();
		double py = centerLocation.getY() - location.getY();
		double pz = centerLocation.getZ() - location.getZ();
		return px * px + py * py + pz * pz;
	}

	private boolean candidateIsValid(Location candidate) {
		if (isOutOfRange(candidate))
			return false;

		if (ignoreList.contains(candidate))
			return false;
		if (vein.contains(candidate))
			return false;
		if (backTrackingQueue.contains(candidate))
			return false;

		if (!Util.minableBlockAt(candidate, minableBlockTypes))
			return false;

		return true;
	}

	private boolean isToBeIncluded(Location location, int dx, int dy, int dz) {
		return location.getY() >= digLocation.getY();
	}
}
