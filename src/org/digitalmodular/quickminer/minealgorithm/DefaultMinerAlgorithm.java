/*
 * This file is part of QuickMiner.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.quickminer.minealgorithm;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.material.MaterialData;

/**
 * A MinerAlgorithm that first attempts to create the largest possible dome, and if there is still room in the vein
 * for more blocks, it repeatedly adds domes that start one block below the previous one.
 *
 * @author Mark Jeronimus
 */
// Created 2016-11-07
public class DefaultMinerAlgorithm implements MinerAlgorithm {
	private final DomeFillAlgorithm domeFiller = new DomeFillAlgorithm();

	@Override
	public List<Location> calculateVein(Location centerLocation,
	                                    Location digLocation,
	                                    int maxCount,
	                                    List<MaterialData> minableBlockTypes) {
		List<Location> vein = new ArrayList<>(maxCount);

		Location nextLocation = digLocation;
		do {
			int remainingCount = maxCount - vein.size();
			List<Location> dome = domeFiller
					.calculateVein(centerLocation, nextLocation, remainingCount, minableBlockTypes);

			vein.addAll(dome);
			domeFiller.addToIgnoreList(dome);

			// In case of DomeFillAlgorithm, the reserveQueue contains the blocks directly below the dome,
			// ordered by distance from centerLocation.
			nextLocation = domeFiller.tryGetFromReserveQueue();
		} while (nextLocation != null);

		return vein;
	}
}
