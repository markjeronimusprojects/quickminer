/*
 * This file is part of QuickMiner.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.quickminer.minealgorithm;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.material.MaterialData;

/**
 * An algorithm that calculates and returns a 'vein' of blocks that can be mined at once. The returned vein is
 * ordered by distance fom the centerLocation (usually the player's waist).
 *
 * @author Mark Jeronimus
 */
// Created 2016-11-06
public interface MinerAlgorithm {

	/**
	 * @param centerLocation The location to base all distance calculations on
	 * @param digLocation    The first block dug, as a starting point for finding connected blocks
	 * @param maxCount       The maximum number of blocks in the vein. The returned vein may contain less blocks, but
	 *                       always at least one.
	 */
	List<Location> calculateVein(Location centerLocation,
	                             Location digLocation,
	                             int maxCount,
	                             List<MaterialData> minableBlockTypes);
}
